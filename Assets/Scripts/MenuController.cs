﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour 
{
	private int contador = 31;
	public Text contadorUI;
	public CardboardModeMgr cardboard;
	// Use this for initialization
	void Start ()
	{
		InvokeRepeating ("Seconds", 0, 1);
	}
	
	// Update is called once per frame
	void Update ()
	{
		contadorUI.text = contador.ToString ();
		if (contador == 0) {
			SceneManager.LoadScene ("VR_LaJ");
		}
			
	}

	void Seconds()
	{
		contador--;
	}
}
