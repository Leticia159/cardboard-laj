﻿using UnityEngine;
using System.Collections;

public class CanWalk : MonoBehaviour 
{
	public MovePlayer player;

	public void ChangeCanWalk()
	{
		player.canWalk = true;
	}
}
