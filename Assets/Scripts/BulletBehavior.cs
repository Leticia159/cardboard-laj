﻿using UnityEngine;
using System.Collections;

public class BulletBehavior : MonoBehaviour
{
	public float speed;

	public float timeToLive;

	// Use this for initialization
	void Start () 
	{
		Destroy (gameObject, timeToLive);
	}
	
	// Update is called once per frame
	void Update ()
	{
		transform.Translate (Vector3.back * speed * Time.deltaTime);

	}
}
