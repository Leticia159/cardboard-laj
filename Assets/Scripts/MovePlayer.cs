﻿using UnityEngine;
using System.Collections;

public class MovePlayer : MonoBehaviour 
{
	private CharacterController player;
	public float speed;
	public bool canWalk = true;

	public Transform[] waypoint;
	private int currentWaypoint = 0;
	private float pauseDuration = 0;
	private float currentTime;

	// Use this for initialization
	void Start ()
	{
		player = this.GetComponent<CharacterController> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (canWalk)
		{
			Patrol ();
			//player.Move (transform.forward * speed * Time.fixedDeltaTime);
		}
		
	}
	void Patrol()
	{

		Vector3 target = waypoint [currentWaypoint].position;
		target.y = transform.position.y;
		Vector3 moveDirection = target - transform.position;

		if ( moveDirection.magnitude < 0.3f)
		{

			if(currentTime == 0.0f)
			{
				currentTime = Time.time;
			}
			if((Time.time - currentTime) >= pauseDuration)
			{
				currentWaypoint++;
				currentTime = 0;
			}
		}
		else
		{
			//Quaternion rotation = Quaternion.LookRotation (target - transform.position);
			//transform.rotation = Quaternion.Slerp (transform.rotation, rotation, Time.deltaTime * dampingLook);
			player.Move (moveDirection.normalized * speed * Time.deltaTime);
		}
	}
}
