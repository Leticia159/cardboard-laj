﻿using UnityEngine;
using System.Collections;

public class EnemyManager : MonoBehaviour 
{
	public GameObject[] enemy;
	public Material[] material;
	public int enemyCount;

	void Update ()
	{

		if (enemyCount < 5)
		{
			int x = Random.Range (20, 450);
			int y = 10;
			int z = Random.Range (20, 470);
			Vector3 spawn = new Vector3 (x, y, z);

			int zombie = Random.Range (0, enemy.Length);
			if (zombie == 0)
			{
				Instantiate (enemy [0], spawn, Quaternion.identity);
				enemy[0].GetComponentInChildren<Renderer> ().material = material [Random.Range (0, material.Length)];
				enemyCount++;
			}
			if (zombie == 1)
			{
				Instantiate (enemy [1], spawn, Quaternion.identity);
				enemy[1].GetComponentInChildren<Renderer>().material = material [Random.Range (0, material.Length)];
				enemyCount++;
			}


		}
	}


}
